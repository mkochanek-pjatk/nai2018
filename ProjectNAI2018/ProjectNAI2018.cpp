// ProjectNAI2018.cpp : Defines the entry point for the console application.


#include "stdafx.h"
#include "Object.h"
#include <sstream>
#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>
#include <vector>


using namespace cv;
using namespace std;

const int window_widht = 1024;
const int window_height = 768;
const string window_name = "Obraz kamery";

//max ilosc obiektow do wykrycia
const int max_objects = 50; 

//minimalny obszar obiektu
const int min_area_of_object = 20 * 20; 

//maksymalny obszar obiektu
const int max_area_of_object = window_widht * window_height / 1.5; 

Mat src;

//zmiana int na string
string int2string(int number) {
	std::stringstream string;
	string << number;
	return string.str();
}

//rysowanie obiektu znalezionego na podstawie koloru
void drawingObject(vector<Object> theObjects, Mat &frame, Mat &temp, vector< vector<Point> > contours, vector<Vec4i> hierarchy) {

	for (int i=0; i<theObjects.size(); i++) {
		//rysowanie konturu
		cv::drawContours(frame, contours, i, theObjects.at(i).getColor(), 3, 8, hierarchy);
		//rysowanie kola
		cv::circle(frame, cv::Point(theObjects.at(i).getXPos(), theObjects.at(i).getYPos()), 5, theObjects.at(i).getColor());
		//dodaje tekst do obiektu
		cv::putText(frame, int2string(theObjects.at(i).getXPos()) + " , " + int2string(theObjects.at(i).getYPos()), cv::Point(theObjects.at(i).getXPos(), theObjects.at(i).getYPos() + 20), 1, 1, theObjects.at(i).getColor());
		cv::putText(frame, theObjects.at(i).getType(), cv::Point(theObjects.at(i).getXPos(), theObjects.at(i).getYPos() - 20), 1, 2, theObjects.at(i).getColor());
	}
}


//sledzenie obiektu
void trackingObject(Object theObject, Mat threshold, Mat HSV, Mat &cameraFeed) {

	vector <Object> objects;

	Mat temp;

	threshold.copyTo(temp);

	//oba wektory poniżej potrzebne dla outputu findContours
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//znajdowanie konturów filtrowanego obrazu z użyciem funkcji wbudowanej w OpenCV findContours 
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	//użycie metody moments do znalezienia filtrowanego obiektu
	double refArea = 0;
	bool objectFound = false;

	if(hierarchy.size() > 0){
		int numObjects = hierarchy.size();

		//jeśli liczna obietów jest większa niż max_objects bedziemy stosowac filter szumu
		if (numObjects<max_objects) {
			for (int index = 0; index >= 0; index = hierarchy[index][0]) {

				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;

				/*
				Jeśli obszar jest mniejszy niż 20px x 20px to wtedy jest to najprawdopodobniej szum
				Jeśli obszar jest taki sam jak 3/2 rozmiaru obrazka, to najprawdopodobjien jest to zły filtr
				Potrzebny jest jedynie obiekt z najwiekszym obszarem co pozwala zabezpieczyc kazdy jego obszar 
				przy kazdej iteracji i porownac z obszarem kolejnej iteracji
				*/

				if (area>min_area_of_object) {

					Object object;
					object.setXPos(moment.m10 / area);
					object.setYPos(moment.m01 / area);
					object.setType(theObject.getType());
					object.setColor(theObject.getColor());
					objects.push_back(object);
					objectFound = true;
				}
				else objectFound = false;
			}

			//jesli znaleziono obiekt, informujemy uzytkownika
			if (objectFound == true) {
				//zaznaczamy lokalizacje obiektu na ekranie
				drawingObject(objects, cameraFeed, temp, contours, hierarchy);
				
			}

		}
		else putText(cameraFeed, "Za duzy szum, zle dostosowany filter", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void morphologyOps(Mat &thresh) {

	//Dylacja i erozja obrazu na podstawie utworzonego elementu 
	//wybrany element to 3px x 3px prostokąt

	/*
	Erozja jest jednym z podstawowych przekształceń morfologicznych. Jej działanie polega na obcinaniu brzegów obiektu na obrazie.
	Dylatacja służy do zamykania małych otworów oraz zatok we wnętrzu figury. 
	Obiekty zwiększają swoją objętość i jeśli dwa lub więcej obiektów położonych jest blisko siebie, zrastają się w większe obiekty. 
	*/

	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(3, 3));

	//dylacja większym elementem co sprawi ze obiekt bedzie lepiej widoczny 
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));

	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}

int main()
{
	Mat cameraMat;
	Mat cameraMatHistory;
	Mat threshold;
	Mat HSV;
	VideoCapture capture;
	capture.open(0);
	capture.set(CV_CAP_PROP_FRAME_WIDTH, window_widht);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, window_height);
	waitKey(1000);
	int framenumber = 0;
	bool running = true;
	while(1){

		framenumber++;
		if (framenumber==2 or framenumber==3) {

			imshow(window_name, cameraMatHistory);
			framenumber = 0;
			
		}
		running = true;
		while (running) {
			framenumber++;
			//obraz z kamery do macierzy
			capture.read(cameraMat);
			src = cameraMat;

			if (!src.data) {
				return -1;
			}

			//konwersja BGR do HSV 
			cvtColor(cameraMat, HSV, COLOR_BGR2HSV);

			Object blue("blue"), yellow("yellow"), red("red"), green("green");

			//na początku szukamy niebieskich obiektów
			cvtColor(cameraMat, HSV, COLOR_BGR2HSV);
			inRange(HSV, blue.getHSVmin(), blue.getHSVmax(), threshold);
			morphologyOps(threshold);
			trackingObject(blue, threshold, HSV, cameraMat);

			//teraz żółtych
			cvtColor(cameraMat, HSV, COLOR_BGR2HSV);
			inRange(HSV, yellow.getHSVmin(), yellow.getHSVmax(), threshold);
			morphologyOps(threshold);
			trackingObject(yellow, threshold, HSV, cameraMat);

			//czerwonych
			cvtColor(cameraMat, HSV, COLOR_BGR2HSV);
			inRange(HSV, red.getHSVmin(), red.getHSVmax(), threshold);
			morphologyOps(threshold);
			trackingObject(red, threshold, HSV, cameraMat);

			//na końcu zielonych
			cvtColor(cameraMat, HSV, COLOR_BGR2HSV);
			inRange(HSV, green.getHSVmin(), green.getHSVmax(), threshold);
			morphologyOps(threshold);
			trackingObject(green, threshold, HSV, cameraMat);


			imshow(window_name, cameraMat);
			cameraMatHistory = cameraMat;

			waitKey(300);
			running = false;
		}

	}
	return 0;
}
